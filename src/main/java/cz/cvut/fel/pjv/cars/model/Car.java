package cz.cvut.fel.pjv.cars.model;

import java.util.UUID;

public class Car {
    private static int numberOfExistingCars;
    private String manufacturer;
    private String modelName;
    private int year;
    private final UUID vinCode;

    public Car(String manufacturer, String modelName, int year) {
        this.manufacturer = manufacturer;
        this.modelName = modelName;
        this.year = year;
        vinCode = UUID.randomUUID();

        ++numberOfExistingCars;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return String.format("%s %s year %d VIN: %s.", manufacturer, modelName, year, vinCode);
    }

    public static int getNumberOfExistingCars() {
        return numberOfExistingCars;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    public boolean equals(Car obj) {
        return obj.vinCode.equals(vinCode);
    }

    @Override
    public int hashCode() {
        return vinCode.hashCode();
    }
}
